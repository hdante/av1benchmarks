# Copyright 2021 Henrique Dante de Almeida
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set datafile separator ";"
set key bottom left
set mytics
set mxtics
set style line 40 linetype -1 lw 0.5 linecolor '#00dddddd'
set grid x, y, my, mx ls 40, ls 40
set palette model HSV defined (0 1 1 0.9, 1 0 1 0.9)
unset colorbox
set title sprintf('%s - %s %s x VMAF', ARG2, ARG3, ARG4) noenhanced
set xlabel ARG4
set ylabel 'VMAF'
plot for [idx=0:ARG5-1] ARG1 index idx using ($19):($7) smooth csplines \
	with lines linecolor palette frac ((idx+1.5)/(ARG5+0.5)) title "", \
	for [idx=0:ARG5-1] ARG1 index idx using ($19):($7) with points \
	linecolor palette frac ((idx+1.5)/(ARG5+0.5)) title columnheader(1)
