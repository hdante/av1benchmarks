# Copyright 2021 Henrique Dante de Almeida
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set datafile separator ';'
array aom[3]
array rav1e[3]
array svtav1[3]
aom[1] = 1
aom[2] = 3
aom[3] = 5
rav1e[1] = 1
rav1e[2] = 5
rav1e[3] = 9
svtav1[1] = 1
svtav1[2] = 4
svtav1[3] = 7

set yrange [70:*]
set xrange noextend
set key bottom
set xlabel 'kbps'
set ylabel 'VMAF'
set style line 40 linetype -1 lw 0.5 linecolor '#00dddddd'
set mytics
set mxtics
set grid x y mx my ls 40

set title sprintf('crowd_run - multiple codecs bitrate x VMAF', ARG2, ARG3) noenhanced

set label 'worse than x264' at 35000, 82 textcolor 'red' center
set label 'better than x264' at 10000, 97.5 textcolor '#008000' center

plot \
	for [i=1:3] 'aom/preset.data' index aom[i] using ($6)/1e3:($7) \
	title sprintf('aom %s', columnheader(1)) linecolor '#ff8020', \
	for [i=1:3] 'rav1e/preset.data' index rav1e[i] using ($6)/1e3:($7) \
	title sprintf('rav1e %s', columnheader(1)) linecolor '#00af00', \
	for [i=1:3] 'svt-av1/preset.data' index svtav1[i] using ($6)/1e3:($7) \
	title sprintf('svt-av1 %s', columnheader(1)) linecolor '#0000e6', \
	for [i=1:3] 'aom/preset.data' index aom[i] using ($6)/1e3:($7) \
	smooth csplines with lines notitle linecolor '#ff8020', \
	for [i=1:3] 'rav1e/preset.data' index rav1e[i] using ($6)/1e3:($7) \
	smooth csplines with lines notitle linecolor '#00af00', \
	for [i=1:3] 'svt-av1/preset.data' index svtav1[i] using ($6)/1e3:($7) \
	smooth csplines with lines notitle linecolor '#0000e6', \
	'x264/preset.data' index 2 using ($6)/1e3:($7) smooth csplines \
	with lines linecolor '#ff0000' \
	linewidth 2 title sprintf('x264 %s', columnheader(1)), \
	'x264/preset.data' index 2 using ($6)/1e3:($7) smooth csplines \
	with filledcurve y=0 fill transparent solid 0.2 linecolor '#ff0000' \
	notitle, \
	'x264/preset.data' index 2 using ($6)/1e3:($7) smooth csplines \
	with filledcurve x=0 fill transparent solid 0.2 linecolor '#00ff00' \
	notitle
