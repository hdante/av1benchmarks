#!/bin/sh
# Copyright 2021 Henrique Dante de Almeida
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

export GNUTERM='svg dynamic'

set -x

gnuplot -c bitrate-vmaf.gnuplot x264/preset.data crowd_run x264 10 > x264/bitrate-vmaf.svg
gnuplot -c bitrate-vmaf.gnuplot svt-av1/preset.data crowd_run svt-av1 9 > svt-av1/bitrate-vmaf.svg
gnuplot -c bitrate-vmaf.gnuplot rav1e/preset.data crowd_run rav1e 11 > rav1e/bitrate-vmaf.svg
gnuplot -c bitrate-vmaf.gnuplot aom/preset.data crowd_run aom 7 > aom/bitrate-vmaf.svg
gnuplot -c bitrate-vmaf-zoom-x264.gnuplot x264/preset.data crowd_run x264 10 > x264/bitrate-vmaf-zoom.svg
gnuplot -c bitrate-vmaf-zoom-svt-av1.gnuplot svt-av1/preset.data crowd_run svt-av1 9 > svt-av1/bitrate-vmaf-zoom.svg
gnuplot -c bitrate-vmaf-zoom-rav1e.gnuplot rav1e/preset.data crowd_run rav1e 11 > rav1e/bitrate-vmaf-zoom.svg
gnuplot -c bitrate-vmaf-zoom-aom.gnuplot aom/preset.data crowd_run aom 7 > aom/bitrate-vmaf-zoom.svg
gnuplot -c q-bitrate.gnuplot x264/preset.data crowd_run x264 crf 10 > x264/q-bitrate.svg
gnuplot -c q-bitrate.gnuplot svt-av1/preset.data crowd_run svt-av1 qp 9 > svt-av1/q-bitrate.svg
gnuplot -c q-bitrate.gnuplot rav1e/preset.data crowd_run rav1e qp 11 > rav1e/q-bitrate.svg
gnuplot -c q-bitrate.gnuplot aom/preset.data crowd_run aom crf 7 > aom/q-bitrate.svg
gnuplot -c q-vmaf.gnuplot x264/preset.data crowd_run x264 crf 10 > x264/q-vmaf.svg
gnuplot -c q-vmaf.gnuplot svt-av1/preset.data crowd_run svt-av1 qp 9 > svt-av1/q-vmaf.svg
gnuplot -c q-vmaf.gnuplot rav1e/preset.data crowd_run rav1e qp 11 > rav1e/q-vmaf.svg
gnuplot -c q-vmaf.gnuplot aom/preset.data crowd_run aom crf 7 > aom/q-vmaf.svg

gnuplot -c preset-bitrate-x264.gnuplot x264/quality.data crowd_run x264 preset 6 > x264/preset-bitrate.svg
gnuplot -c preset-bitrate.gnuplot svt-av1/quality.data crowd_run svt-av1 preset 6 > svt-av1/preset-bitrate.svg
gnuplot -c preset-bitrate.gnuplot rav1e/quality.data crowd_run rav1e speed 14 > rav1e/preset-bitrate.svg
gnuplot -c preset-bitrate.gnuplot aom/quality.data crowd_run aom cpu-used 7 > aom/preset-bitrate.svg
gnuplot -c preset-vmaf-x264.gnuplot x264/quality.data crowd_run x264 preset 6 > x264/preset-vmaf.svg
gnuplot -c preset-vmaf.gnuplot svt-av1/quality.data crowd_run svt-av1 preset 6 > svt-av1/preset-vmaf.svg
gnuplot -c preset-vmaf.gnuplot rav1e/quality.data crowd_run rav1e speed 14 > rav1e/preset-vmaf.svg
gnuplot -c preset-vmaf.gnuplot aom/quality.data crowd_run aom cpu-used 7 > aom/preset-vmaf.svg

gnuplot multiple.gnuplot > multiple.svg
