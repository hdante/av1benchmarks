#!/usr/bin/env python3
# Copyright 2021 Henrique Dante de Almeida
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from os import rename
from sys import argv

from av1benchmarks import CONFIG

replacements = set()
for i in CONFIG.values():
    replacements |= set((x, x[1:]) for x in i.keys() if x.startswith('-'))

for name in argv[1:]:
    newname = name
    for r in replacements:
        newname = newname.replace(r[0], r[1])
    print('%s ⇒ %s' % (name,  newname))
    rename(name, newname)
