#!/usr/bin/env python3
# Copyright 2021 Henrique Dante de Almeida
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import shlex

from glob import glob
from itertools import product
from json import JSONDecodeError, load
from os import chdir, extsep, unlink
from os.path import join, normpath, splitext
from subprocess import run
from sys import argv, stderr

from av1benchmarks import FORMATS, CONFIG, generate_configs, get_file_name

MEDIAINFO = ['mediainfo', '--Output=JSON']

PRESET_MAP = {
        'x264': '-preset',
        'svt-av1': '-preset',
        'rav1e': '-speed',
        'aom': '-cpu-used',
}

QUALITY_MAP = {
        'x264': '-crf',
        'svt-av1': '-qp',
        'rav1e': '-qp',
        'aom': '-crf',
}

DEFAULT = 'default.mkv'

def process_item(name, item):
    for track in item:
        if track['@type'] == 'General':
            container_format = track['Format']
            container_bit_rate = track['OverallBitRate']
            container_size = track['FileSize']
        elif track['@type'] == 'Video':
            video_format = track['Format']
            video_profile = track['Format_Profile']
            video_bit_rate = track['BitRate']
            video_width = track['Width']
            video_height = track['Height']
            video_frame_rate = track['FrameRate']
            video_frame_count = track['FrameCount']
            video_chroma_subsampling = track['ChromaSubsampling']
            video_bit_depth = track['BitDepth']
            video_encoder = track['Encoded_Library']

    vmaf = splitext(name)[0] + extsep + 'json'
    with open(vmaf) as f:
        j = load(f)

    j = j['pooled_metrics']['vmaf']
    vmaf_min = repr(j['min'])
    vmaf_mean = repr(j['mean'])

    return [name, container_format, video_format, video_profile,
            container_bit_rate, video_bit_rate, vmaf_mean, vmaf_min,
            video_width, video_height, video_frame_rate,
            video_frame_count, video_chroma_subsampling,
            video_bit_depth, container_size, video_encoder]

def load_media_info(dir_):
    mediainfo = 'mediainfo.json'

    try:
        with open(mediainfo) as f:
            mediainfo = load(f)
    except (FileNotFoundError, JSONDecodeError):
        dirs = [join(fmt, '*.mkv') for fmt in FORMATS]
        cmd = MEDIAINFO + dirs

        try:
            with open(mediainfo, 'w+') as f:
                print('[%s]$ %s' % (normpath(dir_), ' '.join(cmd)), file=stderr)
                dirs = sum([glob(path) for path in dirs], [])
                cmd = MEDIAINFO + dirs
                run(cmd, stdout=f, check=True)
                f.seek(0)
                mediainfo = load(f)
        except:
            try:
                unlink(mediainfo)
            except:
                pass
            raise

    mediainfo = {x['media']['@ref']: x['media']['track'] for x in mediainfo}
    return mediainfo

def create_sort_map(fmt):
    sort_map = {}
    for k in CONFIG[fmt]:
        for i, v in enumerate(CONFIG[fmt][k]):
            sort_map.setdefault(k, {})
            sort_map[k][v] = i

    return sort_map

def generate_data_file(name, data, sort_key, group_idx, label, default):
    data.sort(key=sort_key)

    with open(name, 'w') as f:
        last = data[0][group_idx]
        print('"%s %s"' % (label, last), file=f)

        for item in data:
            if last != item[group_idx]:
                print('\n\n"%s %s"' % (label, item[group_idx]), file=f)
                last = item[group_idx]

            print(';'.join(str(x) for x in item), file=f)

        print('\n\n"default"', file=f)
        print(';'.join(str(x) for x in default), file=f)

def generate_preset_file(fmt, data, default):
    sort_key = lambda item: (item[-3], item[-1])
    preset = PRESET_MAP[fmt]
    generate_data_file(join(fmt, 'preset.data'), data, sort_key, -4, preset, default)

def generate_quality_file(fmt, data, default):
    sort_key = lambda item: (item[-1], item[-3])
    quality = QUALITY_MAP[fmt]
    generate_data_file(join(fmt, 'quality.data'), data, sort_key, -2, quality, default)

def process_format(fmt, mediainfo, dir_):
    VARIABLES = ('vmaf', 'bitrate')
    sort_map = create_sort_map(fmt)

    d = CONFIG[fmt]
    k = d.keys()
    v = d.values()

    data = []

    for arg, config in generate_configs(fmt):
        name = get_file_name(fmt, config)
        if name in mediainfo:
            item = process_item(name, mediainfo[name])
            param_map = dict(zip(k, arg))
            preset_key = PRESET_MAP[fmt]
            preset_value = sort_map[preset_key][param_map[preset_key]]
            quality_key = QUALITY_MAP[fmt]
            quality_value = sort_map[quality_key][param_map[quality_key]]

            item += [param_map[preset_key], preset_value,
                     param_map[quality_key], quality_value]
            data.append(item)
        else:
            print('warning: not found: %s' % name)

    default_path = join(fmt, DEFAULT)
    default_item = process_item(default_path, mediainfo[default_path])

    generate_preset_file(fmt, data, default_item)
    generate_quality_file(fmt, data, default_item)

def process(dir_):
    chdir(dir_)
    mediainfo = load_media_info(dir_)

    for fmt in FORMATS:
        process_format(fmt, mediainfo, dir_)

def main():
    for dir_ in argv[1:]:
        process(dir_)

if __name__ == '__main__': main()
