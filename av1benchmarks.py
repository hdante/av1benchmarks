#!/usr/bin/env python3
# Copyright 2021 Henrique Dante de Almeida
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import shlex

from itertools import product, chain
from json import JSONDecodeError, dump, load
from os import SCHED_IDLE, environ, extsep, makedirs, rename, sched_param, \
               sched_setscheduler, wait
from os.path import abspath, join, splitext
from subprocess import CalledProcessError, Popen, run
from sys import argv, stderr

FORMATS = ['x264', 'svt-av1', 'rav1e', 'aom']
CODEC = {
        'x264': ['-c:v', 'libx264'],
        'svt-av1': ['-c:v', 'libsvtav1', '-tile_columns', '1', '-tile_rows',
                    '1'],
        'rav1e': ['-c:v', 'librav1e', '-tiles', '4'],
        'aom': ['-c:v', 'libaom-av1', '-row-mt', '1', '-b:v', '0', '-tiles',
                '2x2'],
}
CONFIG = {
        'x264': {
            '-preset': ('placebo', 'veryslow', 'slower', 'slow', 'medium',
                        'fast', 'faster', 'veryfast', 'superfast', 'ultrafast'),
            '-crf': range(14, 34, 3)
        },
        'svt-av1': {
            '-preset': range(0, 9),
            '-qp': range(30, 56, 5),
        },
        'rav1e': {
            '-speed': range(0, 11),
            '-qp': range(60, 201, 10),
        },
        'aom': {
            '-cpu-used': range(0, 7),
            '-crf': range(25, 56, 5),
        },
}

SPEED = {
        'x264': 50,
        'svt-av1': 10,
        'rav1e': 2,
        'aom': 1,
}

FFMPEG = environ.get('FFMPEG', 'ffmpeg')
VMAF_MODEL = environ.get('VMAF_MODEL',
        '/usr/local/share/model/vmaf_v0.6.1.json')
JOBS = int(environ.get('JOBS', 4))

LOSSLESS = ['-map', 'v', '-c:v', 'libx264', '-qp', '0']
CREATE = [FFMPEG, '-y', '-hide_banner', '-nostats', '-v', '25', '-i']
MEASURE = FFMPEG + ' -y -hide_banner -nostats -v warning -i {path} '    \
        '-i {ref} -lavfi libvmaf=log_fmt=json:log_path={log_path}:'     \
        'model_path={model_path}:n_threads=4 -f null -'

class Graph:
    def __init__(self, vertices=None, edges=None):
        if not vertices:
            self.vertices = []
        else:
            self.vertices = vertices
        if not edges:
            self.edges = []
        else:
            self.edges = edges
    def add_vertex(self, v):
        self.vertices.append(v)
    def add_edge(self, v, w):
        self.edges.append((v, w))
    def update(self, graph):
        self.vertices += graph.vertices
        self.edges += graph.edges
    def __str__(self):
        s = '- vertices:'
        for i, v in enumerate(self.vertices):
            s += '\n\t%d) %s' % (i, str(v))
        s += '\n- edges:'
        for x, y in self.edges:
            s += '\n\t(%d, %d)' % (self.vertices.index(x),
                                   self.vertices.index(y))
        return s

class State:
    ARGS = 'args'
    LOSSLESS = 'lossless'
    COMMANDS = 'commands'

    def load(self, input_, target):
        self.path = join(target, 'state')
        try:
            with open(self.path) as s:
                self.state = load(s)
                self.validate(input_, target)
        except (ValueError, JSONDecodeError):
            print('Invalid state file: %s' % self.path)
            raise SystemExit(1)
        except FileNotFoundError:
            self.state = default_state()
            self.set(State.ARGS, [input_, target])
        except OSError as e:
            print('Error loading state file: %s' % e, file=stderr)
            raise SystemExit(1)

    def validate(self, input_, target):
        if len(self.state) < 3: raise ValueError
        if not State.ARGS in self.state: raise ValueError
        if not State.LOSSLESS in self.state: raise ValueError
        if not State.COMMANDS in self.state: raise ValueError
        if self.state[State.ARGS] != [input_, target]: raise ValueError
        if not isinstance(self.state[State.LOSSLESS], str): raise ValueError
        if not self.state[State.LOSSLESS].startswith(target): raise ValueError
        if not isinstance(self.state[State.COMMANDS], list): raise ValueError
        print(self.state[State.COMMANDS])
        for e in self.state[State.COMMANDS]:
            if not isinstance(e, str): raise ValueError

    def check(self, key, value=None):
        v = self.state[key]
        if value:
            if isinstance(v, list): return value in v
            return value == v
        return bool(v)

    def save(self):
        new = self.path + extsep + 'new'
        with open(new, 'w') as n:
            dump(self.state, n, indent=4)

        try:
            rename(new, self.path)
        except OSError as e:
            print('Warning: unable to save state file: %s' % e)

    def set(self, key, value):
        self.state[key] = value
        self.save()

    def append(self, key, value):
        self.state[key].append(value)
        self.save()

def default_state():
    return {
            State.ARGS: [],
            State.LOSSLESS: '',
            State.COMMANDS: [],
    }

def parse_cmdline():
    try:
        input_ = argv[1]
        target = argv[2]
    except IndexError:
        print('Usage: %s <input_video> <output_directory>' % argv[0])
        raise SystemExit(1)

    return abspath(input_), abspath(target)

def create_directories(target):
    makedirs(target, exist_ok=True)
    for fmt in FORMATS:
        path = join(target, fmt)
        makedirs(path, exist_ok=True)

def create_lossless_reference(state, input_, target):
    if state.check(State.LOSSLESS):
        return state.state[State.LOSSLESS]

    out = join(target, 'lossless' + extsep + 'mkv')
    cmd = CREATE + [input_] + LOSSLESS + [out]
    print(shlex.join(cmd), file=stderr)
    run(cmd)
    state.set(State.LOSSLESS, out)
    return out

def remove_prefix(text):
    if text.startswith('--'): return text[2:]
    if text.startswith('-'): return text[1:]
    return text

def get_file_name(fmt, config, target='', ext='mkv'):
    if len(config) == 0:
        name = join(target, fmt, 'default' + extsep + ext)
    else:
        path = [remove_prefix(s) for s in config]
        name = join(target, fmt, '-'.join(path) + extsep + ext)

    return name

def get_encode_cmd(fmt, ref, target, config):
    name = get_file_name(fmt, config, target)
    cmd = CREATE + [ref] + CODEC[fmt] + config + [name]

    return name, cmd

def get_measure_cmd(ref, path):
    log_path = splitext(path)[0] + extsep + 'json'
    model_path = VMAF_MODEL
    cmd = MEASURE.format(path=shlex.quote(path), ref=shlex.quote(ref),
            log_path=shlex.quote(log_path), model_path=shlex.quote(model_path))
    return shlex.split(cmd)

def generate_configs(fmt):
    d = CONFIG[fmt]
    k = d.keys()
    v = d.values()

    for i in product(*v):
        config = [str(param) for param in sum(zip(k, i), tuple())]
        yield i, config

def get_format_commands(fmt, ref, target):
    g = Graph()

    out, default = get_encode_cmd(fmt, ref, target, [])
    g.add_vertex(default)
    measure = get_measure_cmd(ref, out)
    g.add_vertex(measure)
    g.add_edge(measure, default)

    for _, config in generate_configs(fmt):
        out, cmd = get_encode_cmd(fmt, ref, target, config)
        g.add_vertex(cmd)
        measure = get_measure_cmd(ref, out)
        g.add_vertex(measure)
        g.add_edge(measure, cmd)

    return g

def get_commands(state, ref, target):
    commands = Graph()

    for fmt in FORMATS:
        commands.update(get_format_commands(fmt, ref, target))

    done = set(tuple(state.state[State.COMMANDS]))

    commands.vertices = [x for x in commands.vertices
                         if not shlex.join(x) in done]
    commands.edges = [x for x in commands.edges if not shlex.join(x[0]) in done
                      and not shlex.join(x[1]) in done]

    return commands

def execute(state, commands):
    vertices = list(commands.vertices)
    edges = list(commands.edges)
    running = {}

    try:
        while len(vertices) > 0:
            blocked = [x[0] for x in edges]
            runnable = [v for v in vertices if not v in blocked]

            while len(runnable) > 0 and len(running) < JOBS:
                cmd = runnable.pop()
                print('$', shlex.join(cmd), file=stderr)
                p = Popen(cmd)
                running[p.pid] = p
                vertices.remove(cmd)

            pid, r = wait()
            if r != 0:
                raise SystemExit(1)

            cmd = running[pid].args
            del running[pid]
            edges = [e for e in edges if e[1] != cmd]
            state.append(State.COMMANDS, shlex.join(cmd))
    finally:
        for r in running:
            running[r].terminate()

def configure_os_scheduler():
    sched_setscheduler(0, SCHED_IDLE, sched_param(0))

def main():
    state = State()

    input_, target = parse_cmdline()
    configure_os_scheduler()
    create_directories(target)

    state.load(input_, target)

    lossless = create_lossless_reference(state, input_, target)
    commands = get_commands(state, lossless, target)
    execute(state, commands)

if __name__ == '__main__': main()
